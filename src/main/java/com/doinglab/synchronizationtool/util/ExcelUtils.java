package com.doinglab.synchronizationtool.util;

import com.doinglab.synchronizationtool.model.FoodInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class ExcelUtils {
    public static List<FoodInfo> parseExcelFile(InputStream inputStream) throws IOException {
        Workbook workbook = new XSSFWorkbook(inputStream);

        Sheet sheet = workbook.getSheet("FoodInfo");
        Iterator<Row> rows = sheet.iterator();

        List<FoodInfo> foodInfoList = new ArrayList<>();
        int rowNumber = 0;

        while (rows.hasNext()) {
            Row currentRow = rows.next();

            // skip header
            if (rowNumber == 0) {
                rowNumber++;
                continue;
            }
            Iterator<Cell> cellsInRow = currentRow.iterator();

            FoodInfo foodInfo = new FoodInfo();

            int cellIndex = 0;
            while (cellsInRow.hasNext()) {
                Cell currentCell = cellsInRow.next();

                // count every cell
                switch (cellIndex) {
                    case 0:  // foodNumber
//                        foodInfo.setFoodNumber((long) (currentCell.getNumericCellValue()));
                        foodInfo.setFoodNumber((int) (currentCell.getNumericCellValue()));
                        break;
                    case 1: // largeCategory
                        foodInfo.setLargeCategory(currentCell.getStringCellValue());
                        break;
                    case 2: // middleCategory
                        foodInfo.setMiddleCategory(currentCell.getStringCellValue());
                        break;
                    case 3: // predictKey
                        foodInfo.setPredictKey(currentCell.getStringCellValue());
                        break;
                    case 4: // priority
                        foodInfo.setPriarity((int) currentCell.getNumericCellValue());
                        break;
                    case 5: // relatedKeyword
                        foodInfo.setRelatedKeyword(currentCell.getStringCellValue());
                        break;
                    case 6: //foodName
                        foodInfo.setFoodName(currentCell.getStringCellValue());
                        break;
                    case 7: // foodType
                        foodInfo.setFoodType(currentCell.getStringCellValue());
                        break;
                    case 8:
                        foodInfo.setManufacturer(currentCell.getStringCellValue());
                        break;
                    case 9:
                        foodInfo.setEnglishName(currentCell.getStringCellValue());
                        break;
                    case 10:
                        foodInfo.setChineseName(currentCell.getStringCellValue());
                        break;
                    case 11:
                        foodInfo.setJapaneseName(currentCell.getStringCellValue());
                        break;
                    case 12:
                        foodInfo.setFoodSearchClassification((int) currentCell.getNumericCellValue());
                        break;
                    case 13:
                        foodInfo.setSearchKeyword(currentCell.getStringCellValue());
                        break;
                    case 14:
                        foodInfo.setStandardUnit(currentCell.getStringCellValue());
                        break;
                    case 15:
                        foodInfo.setClassification(currentCell.getStringCellValue());
                        break;
                    case 16:
                        foodInfo.setTotalGram(currentCell.getNumericCellValue());
                        break;
                    case 17:
                        foodInfo.setCalorie(currentCell.getNumericCellValue());
                        break;
                    case 18:
                        foodInfo.setCarbonHydrate(currentCell.getNumericCellValue());
                        break;
                    case 19:
                        foodInfo.setSugar(currentCell.getNumericCellValue());
                        break;
                    case 20:
                        foodInfo.setProtein(currentCell.getNumericCellValue());
                        break;
                    case 21:
                        foodInfo.setFat(currentCell.getNumericCellValue());
                        break;
                    case 22:
                        foodInfo.setSaturatedFattyAcid(currentCell.getNumericCellValue());
                        break;
                    case 23:
                        foodInfo.setTransFat(currentCell.getNumericCellValue());
                        break;
                    case 24:
                        foodInfo.setCholesterol(currentCell.getNumericCellValue());
                        break;
                    case 25:
                        foodInfo.setSodium(currentCell.getNumericCellValue());
                        break;
                    case 26:
                        foodInfo.setCellulose(currentCell.getNumericCellValue());
                        break;
                    case 27:
                        foodInfo.setCalcium(currentCell.getNumericCellValue());
                        break;
                    case 28:
                        foodInfo.setFolicAcid(currentCell.getNumericCellValue());
                        break;
                    case 29:
                        foodInfo.setIron(currentCell.getNumericCellValue());
                        break;
                    case 30:
                        foodInfo.setVitaminA(currentCell.getNumericCellValue());
                        break;
                    case 31:
                        foodInfo.setVitaminC(currentCell.getNumericCellValue());
                        break;
                    case 32:
                        foodInfo.setCaffeine(currentCell.getNumericCellValue());
                        break;
                    case 33:
                        foodInfo.setWater(currentCell.getNumericCellValue());
                        break;
                    case 34:
                        foodInfo.setAsh(currentCell.getNumericCellValue());
                        break;
                    case 35:
                        foodInfo.setTotal_amino_acids(currentCell.getNumericCellValue());
                        break;
                    case 36:
                        foodInfo.setEssential_amino_acid(currentCell.getNumericCellValue());
                        break;
                    case 37:
                        foodInfo.setNon_essential_amino_acid(currentCell.getNumericCellValue());
                        break;
                    case 38:
                        foodInfo.setTotal_fatty_acid(currentCell.getNumericCellValue());
                        break;
                    case 39:
                        foodInfo.setTotal_essential_acid(currentCell.getNumericCellValue());
                        break;
                    case 40:
                        foodInfo.setTotal_single_unsaturated_fatty_acid(currentCell.getNumericCellValue());
                        break;
                    case 41:
                        foodInfo.setTotal_multiple_unsaturated_fatty_acid(currentCell.getNumericCellValue());
                        break;
                    case 42:
                        foodInfo.setSodium_chloride_equivalent(currentCell.getNumericCellValue());
                        break;
                    case 43:
                        foodInfo.setMagnesium(currentCell.getNumericCellValue());
                        break;
                    case 44:
                        foodInfo.setPhosphorus(currentCell.getNumericCellValue());
                        break;
                    case 45:
                        foodInfo.setKalium(currentCell.getNumericCellValue());
                        break;
                    case 46:
                        foodInfo.setZinc(currentCell.getNumericCellValue());
                        break;
                    case 47:
                        foodInfo.setCopper(currentCell.getNumericCellValue());
                        break;
                    case 48:
                        foodInfo.setManganese(currentCell.getNumericCellValue());
                        break;
                    case 49:
                        foodInfo.setSelenium(currentCell.getNumericCellValue());
                        break;
                    case 50:
                        foodInfo.setMolybdenum(currentCell.getNumericCellValue());
                        break;
                    case 51:
                        foodInfo.setIodine(currentCell.getNumericCellValue());
                        break;
                    case 52:
                        foodInfo.setRetinol(currentCell.getNumericCellValue());
                        break;
                    case 53:
                        foodInfo.setBeta_carotene(currentCell.getNumericCellValue());
                        break;
                    case 54:
                        foodInfo.setVitaminD(currentCell.getNumericCellValue());
                        break;
                    case 55:
                        foodInfo.setVitaminE(currentCell.getNumericCellValue());
                        break;
                    case 56:
                        foodInfo.setVitaminK1(currentCell.getNumericCellValue());
                        break;
                    case 57:
                        foodInfo.setVitaminB1(currentCell.getNumericCellValue());
                        break;
                    case 58:
                        foodInfo.setVitaminB2(currentCell.getNumericCellValue());
                        break;
                    case 59:
                        foodInfo.setNiacin(currentCell.getNumericCellValue());
                        break;
                    case 60:
                        foodInfo.setPantothenic_acid(currentCell.getNumericCellValue());
                        break;
                    case 61:
                        foodInfo.setVitaminB6(currentCell.getNumericCellValue());
                        break;
                    case 62:
                        foodInfo.setBiotin(currentCell.getNumericCellValue());
                        break;
                    case 63:
                        foodInfo.setVitaminB12(currentCell.getNumericCellValue());
                        break;
                }
                cellIndex++;
            }
            foodInfoList.add(foodInfo);
        }

        // close workbook
        workbook.close();

        return foodInfoList;
    }
}
