package com.doinglab.synchronizationtool.controller;

import com.doinglab.synchronizationtool.model.FoodInfo;
import com.doinglab.synchronizationtool.repository.FoodInfoRepository;
import com.doinglab.synchronizationtool.service.FileService;
import com.doinglab.synchronizationtool.util.CsvUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@RestController
public class FileUploadController {
    private final FoodInfoRepository foodInfoRepository;
    private final FileService fileService;

    public FileUploadController(FoodInfoRepository foodInfoRepository, FileService fileService) {
        this.foodInfoRepository = foodInfoRepository;
        this.fileService = fileService;
    }

    // csv
    @PostMapping(value = "/upload", consumes = "multipart/form-data")
    public void uploadMultipart(@RequestParam("csvFile") MultipartFile file, HttpServletResponse response) throws IOException {
        if (file.isEmpty()) {
            log.warn("tis empty");
        }
        log.info("uploading csv file: " + CsvUtils.read(FoodInfo.class, file.getInputStream()));
        // save
        // foodInfoRepository.saveAll(CsvUtils.read(FoodInfo.class, file.getInputStream()));
        response.sendRedirect("index");
    }

    // excel files
    @PostMapping(value = "/uploadExcel", consumes = "multipart/form-data")
    public void uploadMultipartExcel(@RequestParam("excelFile") MultipartFile file, HttpServletResponse response) throws IOException {
        if (file.isEmpty()) {
            log.warn("file is empty");
        }

        // call fileService to perform and save
        fileService.saveExcelData(file);

        /* uploading excel file into the root folder
        String fileLocation;
        InputStream inputStream = file.getInputStream();
        File currentDir = new File(".");
        String path = currentDir.getAbsolutePath();
        fileLocation = path.substring(0, path.length() - 1) + file.getOriginalFilename();
        FileOutputStream f = new FileOutputStream(fileLocation);
        int ch;
        while ((ch = inputStream.read()) != -1) {
            f.write(ch);
        }
        f.flush();
        f.close();

        log.info("uploading excel file..: " + file.getOriginalFilename());
        */
        response.sendRedirect("/index");
    }

//    @PostMapping(value = "/uploadExcel/{fileId}", consumes = "multipart/form-data")
//    public void uploadExcel(@PathVariable(name = "fileId") String fileId) throws IOException {
//        log.info(fileId);
//    }
}
