package com.doinglab.synchronizationtool.controller;

import com.doinglab.synchronizationtool.model.FileItem;
import com.doinglab.synchronizationtool.repository.FoodInfoRepository;
import com.doinglab.synchronizationtool.service.FileService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Controller
public class MainController {
    private final FoodInfoRepository foodInfoRepository;
    private final FileService fileService;

    private static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);

    private static final String USER_IDENTIFIER_KEY = "FoodInfo_Sync_Tool";

    @Value("${google.oauth.callback.uri}")
    private String CALLBACK_URI;

    @Value("${google.secret.key.path}")
    private Resource gdSecretKeys;

    @Value("${google.credentials.folder.path}")
    private Resource credentialsFolder;

    private GoogleAuthorizationCodeFlow flow;

    @PostConstruct
    public void init() throws IOException {
        GoogleClientSecrets secrets = GoogleClientSecrets.load(JSON_FACTORY,
                new InputStreamReader(gdSecretKeys.getInputStream()));
        flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, secrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(credentialsFolder.getFile())).build();
    }

    public MainController(FoodInfoRepository foodInfoRepository, FileService fileService) {
        this.foodInfoRepository = foodInfoRepository;
        this.fileService = fileService;
    }

    @GetMapping({"/", "/index"})
    public String homePage() throws IOException {
        boolean isUserAuthenticated = false;

        Credential credential = flow.loadCredential(USER_IDENTIFIER_KEY);
        if (credential != null) {
            boolean tokenValid = credential.refreshToken(); // talk to the google server and ask for the accessToken
            // and the refreshToken
            if (tokenValid) {
                isUserAuthenticated = true;
            }
        }

        return isUserAuthenticated ? "dashboard" : "index";
    }

    // google
    @GetMapping("/googleSignIn")
    public void googleSignIn(HttpServletResponse response) throws IOException {
        GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
        String redirectURL = url.setRedirectUri(CALLBACK_URI).setAccessType("offline").build();

        response.sendRedirect(redirectURL);
    }

    @GetMapping("/oauth")
    public String saveAuthorizationCode(HttpServletRequest request) throws IOException {
        String code = request.getParameter("code");
        if (code != null) {
            saveToken(code);

            return "dashboard";
        }
        return "index";
    }

    private void saveToken(String code) throws IOException {
        GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(CALLBACK_URI).execute();
        flow.createAndStoreCredential(response, USER_IDENTIFIER_KEY);
    }

    @GetMapping(value = {"/listFiles"}, produces = {"application/json"})
    public @ResponseBody
    List<FileItem> listFiles() throws IOException {
        Credential credential = flow.loadCredential(USER_IDENTIFIER_KEY);

        Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName("FoodInfo Synchronization Tool").build();

        FileList fileList = drive.files().list().setFields("files(id,name,thumbnailLink)").execute();

        List<FileItem> responseList = new ArrayList<>();
        // convert fileList to the fileItemDTO
        for (File file : fileList.getFiles()) {
            FileItem fileItem = new FileItem();
            fileItem.setId(file.getId());
            fileItem.setName(file.getName());
            fileItem.setThumbnailLink(file.getThumbnailLink());

            responseList.add(fileItem);
        }

        return responseList;
    }

    @PostMapping(value = "/uploadExcel/{fileId}")
    public String uploadExcel(@PathVariable(name = "fileId") String fileId,
                              HttpServletResponse httpServletResponse,
                              Model model) throws Exception {
        Credential credential = flow.loadCredential(USER_IDENTIFIER_KEY);

        Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName("FoodInfo Synchronization Tool").build();

        File file = drive.files().get(fileId).execute();
        log.info("mime type: " + file.getMimeType());

        if (!file.getMimeType().equals("application/vnd.google-apps.spreadsheet")) {
            throw new RuntimeException("wrong file type! please select an excel file");
        } else {
            String range = "Sheet1"; // the worksheet of the excel file. must match
            String valueRenderOption = "FORMATTED_VALUE";
            Sheets sheetsService = createSheetsService();
            ValueRange result =
                    sheetsService.spreadsheets().values().get(file.getId(), range).setValueRenderOption(valueRenderOption).execute();
            List<List<Object>> values = result.getValues();

            for (List<Object> value : values) {
                if (value.size() < 64) {
                    throw new RuntimeException("Empty trailing column. Please put dummy data into the empty cells on " +
                            "the " +
                            "worksheet.");
                }
            }
            fileService.saveExcelDataGoogle(values);

            return "/index";
        }
    }

    private Sheets createSheetsService() throws IOException {
        Credential credential = flow.loadCredential(USER_IDENTIFIER_KEY);

        return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName("FoodInfo " +
                "Synchronization Tool").build();
    }

    @GetMapping("/error")
    private String errorPage(Model model) {
        return "error";
    }
}