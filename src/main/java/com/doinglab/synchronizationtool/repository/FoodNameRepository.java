package com.doinglab.synchronizationtool.repository;

import com.doinglab.synchronizationtool.model.FoodName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

@Transactional(rollbackOn = Exception.class)
public interface FoodNameRepository extends JpaRepository<FoodName, Long> {
//    @Transactional
//    @Modifying
//    @Query("delete from FoodName f where f.foodInfoId in ?1")
//    void deleteAllRows(List<Integer> ids);

    @Transactional
    @Modifying
    @Query("delete from FoodName")
    void deleteAllRows();
}
