package com.doinglab.synchronizationtool.repository;

import com.doinglab.synchronizationtool.model.FoodInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = Exception.class)
public interface FoodInfoRepository extends JpaRepository<FoodInfo, Long> {
}
