package com.doinglab.synchronizationtool.service.impl;

import com.doinglab.synchronizationtool.model.FoodInfo;
import com.doinglab.synchronizationtool.model.FoodName;
import com.doinglab.synchronizationtool.repository.FoodInfoRepository;
import com.doinglab.synchronizationtool.repository.FoodNameRepository;
import com.doinglab.synchronizationtool.service.FileService;
import com.doinglab.synchronizationtool.util.ExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
@Service
public class FileServiceImpl implements FileService {

    private final FoodInfoRepository foodInfoRepository;
    private final FoodNameRepository foodNameRepository;

    public FileServiceImpl(FoodInfoRepository foodInfoRepository, FoodNameRepository foodNameRepository) {
        this.foodInfoRepository = foodInfoRepository;
        this.foodNameRepository = foodNameRepository;
    }

    @Override
    public void saveExcelData(MultipartFile file) throws IOException {
        List<FoodInfo> foodInfoList = ExcelUtils.parseExcelFile(file.getInputStream());
        // save
//        foodInfoRepository.saveAll(foodInfoList);
    }


//    @Transactional
//    @Override
//    public void deleteAllRows(List<Integer> ids) {
//        foodNameRepository.deleteAllRows(ids);
//    }

    @Transactional
    @Override
    public void deleteAllRows() {
        foodNameRepository.deleteAllRows();
    }

    @Override
    @Transactional(rollbackFor = {
            RuntimeException.class,
            IndexOutOfBoundsException.class,
            NumberFormatException.class,
            Exception.class})
    public void saveExcelDataGoogle(List<List<Object>> file) {
        long startTime = System.currentTimeMillis();
        List<FoodInfo> foodInfoList = new ArrayList<>();
        List<FoodName> foodNameList = new ArrayList<>();
        List<Integer> foodNameIds = new ArrayList<>();

        if (file == null || file.isEmpty()) {
            log.error("no data found.");
        } else {
            // skips the header row
            for (List<Object> row : file.subList(1, file.size())) {
                // any weird characters that could be inside the 'number' cell
                String[] wrongFormat = new String[]{",", ";", ":", "'", "\""};
                for (String s : wrongFormat) {
                    // 16 to 64 because those are number formatted cells (left to right)
                    IntStream.range(16, 64)
                            .filter(j -> row.get(j).toString().contains(s))
                            .forEach(j -> {
                                log.info("row: " + row.get(j));
                                throw new NumberFormatException("Wrong number format at column: " + file.get(0).get(j) + "." + System.lineSeparator() + "With value: " + row.get(j));
                            });
                }
                int foodNumber = (Integer.parseInt(row.get(0).toString()));
                String largeCategory = row.get(1).toString();
                String middleCategory = row.get(2).toString();
                String predictKey = row.get(3).toString();

                String relatedKeyword = row.get(5).toString();
                String foodNameString = row.get(6).toString();
                String foodType = row.get(7).toString();
                String manufacturer = row.get(8).toString();
                String englishName = row.get(9).toString();
                String chineseName = row.get(10).toString();
                String japaneseName = row.get(11).toString();
                String searchKeyword = row.get(13).toString();
                String standardUnit = row.get(14).toString();
                String classification = row.get(15).toString();

                FoodInfo foodInfo = new FoodInfo();

                foodInfo.setFoodNumber(foodNumber);

                foodInfo.setLargeCategory(largeCategory);
                foodInfo.setMiddleCategory(middleCategory);
                foodInfo.setPredictKey(predictKey);

                int priority;
                if (row.get(4).equals("")) {
                    priority = 0;
                    foodInfo.setPriarity(priority);
                } else if (row.get(4) != null) {
                    foodInfo.setPriarity(Integer.valueOf((String) row.get(4)));
                }

                double d = 0.0;
                foodInfo.setRelatedKeyword(relatedKeyword);
                foodInfo.setFoodName(foodNameString);
                foodInfo.setFoodType(foodType);
                foodInfo.setManufacturer(manufacturer);

                foodInfo.setEnglishName(englishName);
                foodInfo.setChineseName(chineseName);
                foodInfo.setJapaneseName(japaneseName);

                int foodSearchClassification;
                if (row.get(12).toString().equals("")) { // if it's empty
                    foodSearchClassification = 0;
                    foodInfo.setFoodSearchClassification(foodSearchClassification);
                } else if (row.get(12) == null) {
                    foodInfo.setFoodSearchClassification(Integer.parseInt(row.get(12).toString()));
                }

                foodInfo.setSearchKeyword(searchKeyword);
                foodInfo.setStandardUnit(standardUnit);
                foodInfo.setClassification(classification);

                // whatever I have to parse to Double from String, need to check if it's empty.
                // if empty, set it to 0
                if (!row.get(16).toString().equals("")) {
                    foodInfo.setTotalGram(Double.parseDouble(row.get(16).toString()));
                } else {
                    foodInfo.setTotalGram(d);
                }
                if (!row.get(17).toString().equals("")) {
                    foodInfo.setCalorie(Double.parseDouble(row.get(17).toString()));
                } else {
                    foodInfo.setCalorie(d);
                }
                if (!row.get(18).toString().equals("")) {
                    foodInfo.setCarbonHydrate(Double.parseDouble(row.get(18).toString()));
                } else {
                    foodInfo.setCarbonHydrate(d);
                }
                if (!row.get(19).toString().equals("")) {
                    foodInfo.setSugar(Double.parseDouble(row.get(19).toString()));
                } else {
                    foodInfo.setSugar(d);
                }
                if (!row.get(20).toString().equals("")) {
                    foodInfo.setProtein(Double.parseDouble(row.get(20).toString()));
                } else {
                    foodInfo.setProtein(d);
                }
                if (!row.get(21).toString().equals("")) {
                    foodInfo.setFat(Double.parseDouble(row.get(21).toString()));
                } else {
                    foodInfo.setFat(d);
                }
                if (!row.get(22).toString().equals("")) {
                    foodInfo.setSaturatedFattyAcid(Double.parseDouble(row.get(22).toString()));
                } else {
                    foodInfo.setSaturatedFattyAcid(d);
                }
                if (!row.get(23).toString().equals("")) {
                    foodInfo.setTransFat(Double.parseDouble(row.get(23).toString()));
                } else {
                    foodInfo.setTransFat(d);
                }
                if (!row.get(24).toString().equals("")) {
                    foodInfo.setCholesterol(Double.parseDouble(row.get(24).toString()));
                } else {
                    foodInfo.setCholesterol(d);
                }
                if (!row.get(25).toString().equals("")) {
                    foodInfo.setSodium(Double.parseDouble(row.get(25).toString()));
                } else {
                    foodInfo.setSodium(d);
                }
                if (!row.get(26).toString().equals("")) {
                    foodInfo.setCellulose(Double.parseDouble(row.get(26).toString()));
                } else {
                    foodInfo.setCellulose(d);
                }
                if (!row.get(27).toString().equals("")) {
                    foodInfo.setCalcium(Double.parseDouble(row.get(27).toString()));
                } else {
                    foodInfo.setCalcium(d);
                }
                if (!row.get(28).toString().equals("")) {
                    foodInfo.setFolicAcid(Double.parseDouble(row.get(28).toString()));
                } else {
                    foodInfo.setFolicAcid(d);
                }
                if (!row.get(29).toString().equals("")) {
                    foodInfo.setIron(Double.parseDouble(row.get(29).toString()));
                } else {
                    foodInfo.setIron(d);
                }
                if (!row.get(30).toString().equals("")) {
                    foodInfo.setVitaminA(Double.parseDouble(row.get(30).toString()));
                } else {
                    foodInfo.setVitaminA(d);
                }
                if (!row.get(31).toString().equals("")) {
                    foodInfo.setVitaminC(Double.parseDouble(row.get(31).toString()));
                } else {
                    foodInfo.setVitaminC(d);
                }
                if (!row.get(32).toString().equals("")) {
                    foodInfo.setCaffeine(Double.parseDouble(row.get(32).toString()));
                } else {
                    foodInfo.setCaffeine(d);
                }
                if (!row.get(33).toString().equals("")) {
                    foodInfo.setWater(Double.parseDouble(row.get(33).toString()));
                } else {
                    foodInfo.setWater(d);
                }
                if (!row.get(34).toString().equals("")) {
                    foodInfo.setAsh(Double.parseDouble(row.get(34).toString()));
                } else {
                    foodInfo.setAsh(d);
                }
                if (!row.get(35).toString().equals("")) {
                    foodInfo.setTotal_amino_acids(Double.parseDouble(row.get(35).toString()));
                } else {
                    foodInfo.setTotal_amino_acids(d);
                }
                if (!row.get(36).toString().equals("")) {
                    foodInfo.setEssential_amino_acid(Double.parseDouble(row.get(36).toString()));
                } else {
                    foodInfo.setEssential_amino_acid(d);
                }
                if (!row.get(37).toString().equals("")) {
                    foodInfo.setNon_essential_amino_acid(Double.parseDouble(row.get(37).toString()));
                } else {
                    foodInfo.setNon_essential_amino_acid(d);
                }
                if (!row.get(38).toString().equals("")) {
                    foodInfo.setTotal_fatty_acid(Double.parseDouble(row.get(38).toString()));
                } else {
                    foodInfo.setTotal_fatty_acid(d);
                }
                if (!row.get(39).toString().equals("")) {
                    foodInfo.setTotal_essential_acid(Double.parseDouble(row.get(39).toString()));
                } else {
                    foodInfo.setTotal_essential_acid(d);
                }
                if (!row.get(40).toString().equals("")) {
                    foodInfo.setTotal_single_unsaturated_fatty_acid(Double.parseDouble(row.get(40).toString()));
                } else {
                    foodInfo.setTotal_single_unsaturated_fatty_acid(d);
                }
                if (!row.get(41).toString().equals("")) {
                    foodInfo.setTotal_multiple_unsaturated_fatty_acid(Double.parseDouble(row.get(41).toString()));
                } else {
                    foodInfo.setTotal_multiple_unsaturated_fatty_acid(d);
                }
                if (!row.get(42).toString().equals("")) {
                    foodInfo.setSodium_chloride_equivalent(Double.parseDouble(row.get(42).toString()));
                } else {
                    foodInfo.setSodium_chloride_equivalent(d);
                }
                if (!row.get(43).toString().equals("")) {
                    foodInfo.setMagnesium(Double.parseDouble(row.get(43).toString()));
                } else {
                    foodInfo.setMagnesium(d);
                }
                if (!row.get(44).toString().equals("")) {
                    foodInfo.setPhosphorus(Double.parseDouble(row.get(44).toString()));
                } else {
                    foodInfo.setPhosphorus(d);
                }
                if (!row.get(45).toString().equals("")) {
                    foodInfo.setKalium(Double.parseDouble(row.get(45).toString()));
                } else {
                    foodInfo.setKalium(d);
                }
                if (!row.get(46).toString().equals("")) {
                    foodInfo.setZinc(Double.parseDouble(row.get(46).toString()));
                } else {
                    foodInfo.setZinc(d);
                }
                if (!row.get(47).toString().equals("")) {
                    foodInfo.setCopper(Double.parseDouble(row.get(47).toString()));
                } else {
                    foodInfo.setCopper(d);
                }
                if (!row.get(48).toString().equals("")) {
                    foodInfo.setManganese(Double.parseDouble(row.get(48).toString()));
                } else {
                    foodInfo.setManganese(d);
                }
                if (!row.get(49).toString().equals("")) {
                    foodInfo.setSelenium(Double.parseDouble(row.get(49).toString()));
                } else {
                    foodInfo.setSelenium(d);
                }
                if (!row.get(50).toString().equals("")) {
                    foodInfo.setMolybdenum(Double.parseDouble(row.get(50).toString()));
                } else {
                    foodInfo.setMolybdenum(d);
                }
                if (!row.get(51).toString().equals("")) {
                    foodInfo.setIodine(Double.parseDouble(row.get(51).toString()));
                } else {
                    foodInfo.setIodine(d);
                }
                if (!row.get(52).toString().equals("")) {
                    foodInfo.setRetinol(Double.parseDouble(row.get(52).toString()));
                } else {
                    foodInfo.setRetinol(d);
                }
                if (!row.get(53).toString().equals("")) {
                    foodInfo.setBeta_carotene(Double.parseDouble(row.get(53).toString()));
                } else {
                    foodInfo.setBeta_carotene(d);
                }
                if (!row.get(54).toString().equals("")) {
                    foodInfo.setVitaminD(Double.parseDouble(row.get(54).toString()));
                } else {
                    foodInfo.setVitaminD(d);
                }
                if (!row.get(55).toString().equals("")) {
                    foodInfo.setVitaminE(Double.parseDouble(row.get(55).toString()));
                } else {
                    foodInfo.setVitaminE(d);
                }
                if (!row.get(56).toString().equals("")) {
                    foodInfo.setVitaminK1(Double.parseDouble(row.get(56).toString()));
                } else {
                    foodInfo.setVitaminK1(d);
                }
                if (!row.get(57).toString().equals("")) {
                    foodInfo.setVitaminB1(Double.parseDouble(row.get(57).toString()));
                } else {
                    foodInfo.setVitaminB1(d);
                }
                if (!row.get(58).toString().equals("")) {
                    foodInfo.setVitaminB2(Double.parseDouble(row.get(58).toString()));
                } else {
                    foodInfo.setVitaminB2(d);
                }
                if (!row.get(59).toString().equals("")) {
                    foodInfo.setNiacin(Double.parseDouble(row.get(59).toString()));
                } else {
                    foodInfo.setNiacin(d);
                }
                if (!row.get(60).toString().equals("")) {
                    foodInfo.setPantothenic_acid(Double.parseDouble(row.get(60).toString()));
                } else {
                    foodInfo.setPantothenic_acid(d);
                }
                if (!row.get(61).toString().equals("")) {
                    foodInfo.setVitaminB6(Double.parseDouble(row.get(61).toString()));
                } else {
                    foodInfo.setVitaminB6(d);
                }
                if (!row.get(62).toString().equals("")) {
                    foodInfo.setBiotin(Double.parseDouble(row.get(62).toString()));
                } else {
                    foodInfo.setBiotin(d);
                }
                if (!row.get(63).toString().equals("")) {
                    foodInfo.setVitaminB12(Double.parseDouble(row.get(63).toString()));
                } else {
                    foodInfo.setVitaminB12(d);
                }

                FoodName foodName = new FoodName();

                foodName.setFoodInfoId(foodNumber);
                foodName.setFoodName(foodNameString);
                foodName.setManufacturer(manufacturer);
                foodName.setPredictKey(predictKey);

                foodNameList.add(foodName);
                if (!englishName.equals("")) {
                    FoodName foodName1 = new FoodName();
                    foodName1.setFoodInfoId(Integer.parseInt(row.get(0).toString()));
                    foodName1.setManufacturer(manufacturer);
                    foodName1.setFoodName(englishName);
                    foodName1.setPredictKey(predictKey);

                    foodNameList.add(foodName1);
                }
                if (!chineseName.equals("")) {
                    FoodName foodName2 = new FoodName();
                    foodName2.setFoodInfoId(Integer.parseInt(row.get(0).toString()));
                    foodName2.setManufacturer(manufacturer);
                    foodName2.setFoodName(chineseName);
                    foodName2.setPredictKey(predictKey);

                    foodNameList.add(foodName2);
                }
                if (!japaneseName.equals("")) {
                    FoodName foodName3 = new FoodName();
                    foodName3.setFoodInfoId(Integer.parseInt(row.get(0).toString()));
                    foodName3.setManufacturer(manufacturer);
                    foodName3.setFoodName(japaneseName);
                    foodName3.setPredictKey(predictKey);

                    foodNameList.add(foodName3);
                }

                foodInfoList = Collections.singletonList(foodInfo);
            }
        }
        for (FoodName foodName : foodNameList) {
            foodNameIds.add(foodName.getFoodInfoId());
        }
        if (!foodNameList.isEmpty()) {
            log.info("deleting all rows..");
//            deleteAllRows(foodNameIds);
            deleteAllRows();
            foodNameRepository.flush();
            log.info("delete process complete");
        }
        log.info("saving foodInfos..");
        foodInfoRepository.saveAll(foodInfoList);
        foodInfoRepository.flush();
        log.info("foodInfos save process complete");
        log.info("saving foodNames..");
        foodNameRepository.saveAll(foodNameList);
        foodNameRepository.flush();
        log.info("foodNames save process complete");

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        String s = String.format("%d minutes and %d seconds",
                TimeUnit.MILLISECONDS.toMinutes(elapsedTime),
                TimeUnit.MILLISECONDS.toSeconds(elapsedTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsedTime)));
        System.out.println("sync process completed, took " + s + ".");
    }
}
