package com.doinglab.synchronizationtool.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileService {
    void saveExcelData(MultipartFile file) throws IOException;
    void saveExcelDataGoogle(List<List<Object>> file) throws IOException;
//    void deleteAllRows(List<Integer> ids);
    void deleteAllRows();
}