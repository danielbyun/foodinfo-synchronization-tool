package com.doinglab.synchronizationtool.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "FoodInfo")
public class FoodInfo {
    @Id
    @Column(name = "FoodNumber")
    private int foodNumber;

    @Column(name = "LargeCategory")
    private String largeCategory;

    @Column(name = "MiddleCategory")
    private String middleCategory;

    @Column(name = "PredictKey")
    private String predictKey;

    @Column(name = "Priarity")
    private int priarity;

    @Column(name = "RelatedKeyword")
    private String relatedKeyword;

    @Column(name = "FoodName")
    private String foodName;

    @Column(name = "FoodType")
    private String foodType;

    @Column(name = "Manufacturer")
    private String manufacturer;

    @Column(name = "EnglishName")
    private String englishName;

    @Column(name = "ChineseName")
    private String chineseName;

    @Column(name = "JapaneseName")
    private String japaneseName;

    @Column(name = "FoodSearchClassification")
    private int foodSearchClassification;

    @Column(name = "SearchKeyword")
    private String searchKeyword;

    @Column(name = "StandardUnit")
    private String standardUnit;

    @Column(name = "Classification")
    private String classification;

    @Column(name = "TotalGram")
    private Double totalGram;

    @Column(name = "Calorie")
    private Double calorie;

    @Column(name = "CarbonHydrate")
    private Double carbonHydrate;

    @Column(name = "Sugar")
    private Double sugar;

    @Column(name = "Protein")
    private Double protein;

    @Column(name = "Fat")
    private Double fat;

    @Column(name = "SaturatedFattyAcid")
    private Double saturatedFattyAcid;

    @Column(name = "TransFat")
    private Double transFat;

    @Column(name = "Cholesterol")
    private Double cholesterol;

    @Column(name = "Sodium")
    private Double sodium;

    @Column(name = "Cellulose")
    private Double cellulose;

    @Column(name = "Calcium")
    private Double calcium;

    @Column(name = "FolicAcid")
    private Double folicAcid;

    @Column(name = "Iron")
    private Double iron;

    @Column(name = "VitaminA")
    private Double vitaminA;

    @Column(name = "VitaminC")
    private Double vitaminC;

    @Column(name = "Caffeine")
    private Double caffeine;

    @Column(name = "water")
    private Double water;

    @Column(name = "ash")
    private Double ash;

    @Column(name = "total_amino_acids")
    private Double total_amino_acids;

    @Column(name = "essential_amino_acid")
    private Double essential_amino_acid;

    @Column(name = "non_essential_amino_acid")
    private Double non_essential_amino_acid;

    @Column(name = "total_fatty_acid")
    private Double total_fatty_acid;

    @Column(name = "total_essential_acid")
    private Double total_essential_acid;

    @Column(name = "total_single_unsaturated_fatty_acid")
    private Double total_single_unsaturated_fatty_acid;

    @Column(name = "total_multiple_unsaturated_fatty_acid")
    private Double total_multiple_unsaturated_fatty_acid;

    @Column(name = "sodium_chloride_equivalent")
    private Double sodium_chloride_equivalent;

    @Column(name = "magnesium")
    private Double magnesium;

    @Column(name = "phosphorus")
    private Double phosphorus;

    @Column(name = "kalium")
    private Double kalium;

    @Column(name = "zinc")
    private Double zinc;

    @Column(name = "copper")
    private Double copper;

    @Column(name = "manganese")
    private Double manganese;

    @Column(name = "selenium")
    private Double selenium;

    @Column(name = "molybdenum")
    private Double molybdenum;

    @Column(name = "iodine")
    private Double iodine;

    @Column(name = "retinol")
    private Double retinol;

    @Column(name = "beta_carotene")
    private Double beta_carotene;

    @Column(name = "vitaminD")
    private Double vitaminD;

    @Column(name = "vitaminE")
    private Double vitaminE;

    @Column(name = "vitaminK1")
    private Double vitaminK1;

    @Column(name = "vitaminB1")
    private Double vitaminB1;

    @Column(name = "vitaminB2")
    private Double vitaminB2;

    @Column(name = "niacin")
    private Double niacin;

    @Column(name = "pantothenic_acid")
    private Double pantothenic_acid;

    @Column(name = "vitaminB6")
    private Double vitaminB6;

    @Column(name = "biotin")
    private Double biotin;

    @Column(name = "vitaminB12")
    private Double vitaminB12;
}