package com.doinglab.synchronizationtool.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@IdClass(FoodName.IdClass.class)
@Table(name = "FoodName")
public class FoodName {
    @Id
    @Column(name = "FoodInfo_id")
    private int foodInfoId;

    @Column(name = "FoodNameType")
    private int foodNameType;

    @Id
    @Column(name = "FoodName")
    private String foodName;

    @Column(name = "Manufacturer")
    private String manufacturer;

    @Column(name = "PredictKey")
    private String predictKey;

    @Data
    static class IdClass implements Serializable {
        private int foodInfoId;
        private String foodName;
    }
}
