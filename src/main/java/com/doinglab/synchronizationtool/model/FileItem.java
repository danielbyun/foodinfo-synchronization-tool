package com.doinglab.synchronizationtool.model;

import java.io.Serializable;

public class FileItem implements Serializable {
    private static final long serialVersionId = 1L;
    private String name;
    private String id;
    private String thumbnailLink;

    public FileItem() {
    }

    public FileItem(String name, String id, String thumbnailLink) {
        this.name = name;
        this.id = id;
        this.thumbnailLink = thumbnailLink;
    }

    public static long getSerialVersionId() {
        return serialVersionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    @Override
    public String toString() {
        return "FileItem{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", thumbnailLink='" + thumbnailLink + '\'' +
                '}';
    }
}
