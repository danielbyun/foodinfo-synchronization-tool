package com.doinglab.synchronizationtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SynchronizationToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(SynchronizationToolApplication.class, args);
    }

}
