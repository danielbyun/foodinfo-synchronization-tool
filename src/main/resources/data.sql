use DietDiary;

insert into FoodInfo (FoodNumber, LargeCategory, MiddleCategory, PredictKey, Priarity, RelatedKeyword, FoodName,
                      FoodType, Manufacturer, EnglishName, ChineseName, JapaneseName, FoodSearchClassification,
                      SearchKeyword, StandardUnit, Classification, TotalGram, Calorie, CarbonHydrate, Sugar, Protein,
                      Fat, SaturatedFattyAcid, TransFat, Cholesterol, Sodium, Cellulose, Calcium, FolicAcid, Iron,
                      VitaminA, VitaminC, Caffeine, water, ash, total_amino_acids, essential_amino_acid,
                      non_essential_amino_acid, total_fatty_acid, total_essential_acid,
                      total_single_unsaturated_fatty_acid, total_multiple_unsaturated_fatty_acid,
                      sodium_chloride_equivalent, magnesium, phosphorus, kalium, zinc, copper, manganese, selenium,
                      molybdenum, iodine, retinol, beta_carotene, vitaminD, vitaminE, vitaminK1, vitaminB1, vitaminB2,
                      niacin, pantothenic_acid, vitaminB6, biotin, vitaminB12)
values (1000, 'B', 'A', 'test', 0, '', '테스트', 'test', '', '', '', '', 2, 'test', 'test', 'test', 100, 20.3, 5.352, 3.235,
        5.323, 5.235, 1.532, 0, 16.235, 4103.235, 1.5, 235.93, 235, 1.95320, 1, 7.23857, 0, 58.2384, 1.5873, 5323.382,
        5938.82, 2958.83, 23258.83, 5.283, 0.583, 2.5831, 0.5382, 1.35980, 39.82, 88.283, 218.382, 1.48, 0.03, 0.165,
        6.31, 10.06, 0.38, 3, 0.582, 0, 1.238, 0, 0.8203, 0.18342, 0.8325, 0.2032, 0.0135, 0.95);