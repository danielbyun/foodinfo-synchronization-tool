let excelFormTrigger = $("#excelFormTrigger");
let csvFormTrigger = $("#csvFormTrigger");
let csvFileUploadForm = $("#csvFileUploadForm");
let excelFileUploadForm = $("#excelFileUploadForm");
let loadingGif = $("#image");
csvFormTrigger.hide();
csvFileUploadForm.hide();
excelFileUploadForm.hide();

// $("#csvFormTrigger").click(function () {
//     $("#csvFormTrigger").prop("disabled", true);
//     csvFileUploadForm.show();
//     excelFileUploadForm.hide();
//     csvFileUploadForm.hide();
// });

excelFormTrigger.click(function () {
    $("#excelFormTrigger").prop("disabled", true);
    excelFileUploadForm.show();
    csvFileUploadForm.hide();
});

$("#refreshFileButton").click(function () {
    $.ajax({
        url: '/listFiles',
    }).done(function (data) { // using done instead of success
        let fileHTML = "";
        for (file of data) {
            fileHTML += '<li class="list-group-item"><img src="' + file.thumbnailLink + '">'
                + file.name + ' (FileID : ' + file.id + ')'
                + '<button  class="btn btn-danger" onclick="uploadExcel(\'' + file.id + '\')">Upload</button></li>';

            "<img src='" + file.thumbnailLink + "' alt='file thumbnail'>"
            // fileHTML +=
            //     "<li class='list-group-item'>" +
            //     +"<p class='title'>" + file.name + "</p>"
            //     + '<small style="margin: 1rem;"> (File ID: ' + file.id + ")</small>"
            //     // + "<input id='number' type='text' placeholder='ending cell number'>"
            //     + "<button class='btn btn-danger' onclick='uploadExcel(\"" + file.id + "\")'>Upload</button>"
            //     + "</li>"
        }

        $("#fileListContainer").html(fileHTML);
    });
});

let errorMessage = $("#errorMessage");

function uploadExcel(id) {
    $(loadingGif).css({
        "display": "block",
        "z-index": "999",
        "width": "10%",
        "margin": "0 auto"
    });

    $.ajax({
        url: "/uploadExcel/" + id,
        method: "POST",
    }).done(function (data) { // success
        alert("Synchronization process completed!");
        location.href = "/index";
    }).fail(function (data) { // fail
        $(loadingGif).css("display", "none");

        $(loadingGif).hide();
        $(errorMessage).prop("hidden", false);
        $(errorMessage).html(data.responseJSON.message);
        // setTimeout(function () {
        //     $(errorMessage).prop("hidden", true);
        // });
    })
}